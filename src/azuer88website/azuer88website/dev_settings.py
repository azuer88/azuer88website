from .settings import *

DEBUG = True

TIME_ZONE = 'UTC'

MIDDLEWARE = ['debug_toolbar.middleware.DebugToolbarMiddleware'] + MIDDLEWARE
INSTALLED_APPS += [
    'debug_toolbar',
]


INTERNAL_IPS = ['127.0.0.1', ]

BOOSTRAP_LOCAL=STATIC_URL + 'bootstrap-3.4.0-dist/'
BOOTSTRAP3['css_url'] = BOOSTRAP_LOCAL + 'css/bootstrap.min.css'
BOOTSTRAP3['javascript_url'] = BOOSTRAP_LOCAL + 'js/bootstrap.min.js'
BOOTSTRAP3['jquery_url'] = BOOSTRAP_LOCAL + 'js/jquery.min.js'