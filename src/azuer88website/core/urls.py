from django.urls import path, include
from . import views

app_name = 'main'

urlpatterns = [
    path('hello/', views.hello_page, name='test_hello'),
]